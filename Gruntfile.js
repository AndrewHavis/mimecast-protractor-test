'use strict';

module.exports = (grunt) => {
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({

        protractor_webdriver: {
            test: {
                options: {
                    path: __dirname + '/node_modules/webdriver-manager/bin',
                    command: 'webdriver-manager start'
                }
            }
        },

        protractor: {
            test: {
                configFile: __dirname + '/protractor.conf.js',
            }
        }

    });

    grunt.registerTask('default', [ 'test' ]);
    grunt.registerTask('test', [ 'selenium_start', 'protractor:test', 'selenium_stop' ]);

};
