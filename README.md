# Mimecast Protractor Test

## Installation
To install the tests, simply clone this repository from Bitbucket in the usual way, and then run `npm install` to install the dependencies. You will also need the `grunt-cli` and `protractor` modules installed globally. This can be done with the following commands:

    npm install grunt-cli -g
    npm install protractor -g
    
You will also need a `credentials.json` file saved in the root, with your Mimecast Secure Messaging username and password. You may also need a token, which can be found in the URL of the login page as the `tkn` parameter. The `credentials.json` file has the following syntax:

    {
        "username": "<username/email address>",
        "password": "<password>",
        "token": "<login token>"
    }
    
## Running

To run the tests, simply issue the `grunt` command. The names and results of the test cases will be shown in the console.