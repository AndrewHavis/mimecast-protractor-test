'use strict';

exports.config = {

  // Parameters (e.g. credentials)
  params: {
      credentials: require('./credentials.json')
  },

  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'firefox'
  },

  // Specify our base URL
  baseUrl: 'https://login-alpha.mimecast.com/m/secure/login/',

  // Spec patterns are relative to the configuration file location passed
  // to protractor (in this example conf.js).
  // They may include glob patterns.
  specs: ['tests/*.spec.js'],

  // Test preparation
  onPrepare: () => {

      browser.get(browser.baseUrl);

        // Unlike Jasmine v1, Jasmine2 doesn't support outputting the test names via isVerbose
        // We will therefore enable the verbose output here instead
        var SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({
            displayStacktrace: false
        }));

  },

  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    showColors: true, // Use colors in the command line report.
    defaultTimeoutInterval: 30000 // Set timeout to 30 seconds
  }
};
