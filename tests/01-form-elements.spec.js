'use strict';

// Import required libraries
const elements = require('./libraries/elements').elements;

describe('CASE 1: Initial form elements', () => {

    it('must have an email address input field visible and enabled', () => {
        expect(elements.inputEmail.isDisplayed()).toBeTruthy();
        expect(elements.inputEmail.isEnabled()).toBeTruthy();
    });

    it('must have a \'Next\' button visible but disabled', () => {
        expect(elements.btnNext.isDisplayed()).toBeTruthy();
        expect(elements.btnNext.isEnabled()).toBeFalsy();
    });

});
