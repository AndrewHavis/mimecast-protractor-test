'use strict';

// Import required libraries
const elements = require('./libraries/elements').elements;
const email = require('./libraries/email-addresses');

describe('CASE 2: Form elements after a valid email address has been submitted', () => {

    // Initialise variables
    let emailAddress = email.generateEmailAddress();

    beforeAll(() => {
        // Refresh the page
        browser.get(browser.baseUrl);
    });

    it('must enter a valid email address', () => {

        // Generate a valid email address and add it to our input field
        elements.inputEmail.sendKeys(emailAddress);

        // Verify the email address is valid
        elements.inputEmail.getAttribute('value').then((emailText) => {
            expect(email.validateEmailAddress(emailText)).toBeTruthy();
        });

    });

    it('must have the \'Next\' button enabled and click on it', () => {

        expect(elements.btnNext.isEnabled()).toBeTruthy();

        // Check the button text is 'Next'
        elements.btnNext.getText().then((btnText) => {
           expect(btnText).toBe('Next');
        });

        elements.btnNext.click();

    });

    it('must now have the password and authentication type fields visible and enabled', () => {
        expect(elements.password.isDisplayed()).toBeTruthy();
        expect(elements.password.isEnabled()).toBeTruthy();
        expect(elements.authType.isDisplayed()).toBeTruthy();
        expect(elements.authType.isEnabled()).toBeTruthy();
    });

    it('must have the email address field disabled, but with the correct email address in it', () => {

        // Check the email field is disabled
        expect(elements.inputEmail.isEnabled()).toBeFalsy();

        // Check the email address displayed within it
        elements.inputEmail.getAttribute('value').then((emailAddressText) => {
            expect(emailAddressText).toBe(emailAddress);
        });

    });

    it('must remove the password field and re-enable the email address field when the \'Login as a different user\' link is clicked', () => {

        // Check the link text is correct
        elements.linkDiffUser.getText().then((linkDiffUserText) => {
            expect(linkDiffUserText).toBe('Log in as a different user.');
        });

        // Click the link
        elements.linkDiffUser.click();

        // Check the password and authentication type fields have been removed, and that the email address field has been re-enabled
        expect(elements.password.isPresent()).toBeFalsy();
        expect(elements.authType.isPresent()).toBeFalsy();
        expect(elements.inputEmail.isEnabled()).toBeTruthy();

        // Check that the email address field is empty
        expect(elements.inputEmail.getText()).toBe('');

    });

    it('must click the \'Forgot your password\' link and go to the reset page', () => {

        // To get the forgotten password link, enter another email address and click the 'Next' button again
        emailAddress = email.generateEmailAddress(); // Save the email address so we can check against it later
        elements.inputEmail.sendKeys(emailAddress);
        elements.btnNext.click();

        // Is the forgotten password link visible?
        expect(elements.linkForgotPass.isDisplayed()).toBeTruthy();

        // Check the link text
        elements.linkForgotPass.getText().then((linkForgotPassText) => {
           expect(linkForgotPassText).toBe('Forgot your password?');
        });

        // Click the link
        elements.linkForgotPass.click();

        // Now check all the relevant elements are visible
        expect(elements.inputResetEmail.isDisplayed()).toBeTruthy();
        expect(elements.btnResetPassword.isDisplayed()).toBeTruthy();
        expect(elements.linkNeverMind.isDisplayed()).toBeTruthy();

        // Check that the correct email address is displayed in the input field
        elements.inputResetEmail.getAttribute('value').then((resetEmailText) => {
           expect(resetEmailText).toBe(emailAddress);
        });

        // Now check the 'Never mind' link text
        elements.linkNeverMind.getText().then((linkNeverMindText) => {
           expect(linkNeverMindText).toBe('Never mind, take me back to the login page.');
        });

    });

    it('must click the \'Never mind\' link to go back to the login page', () => {

        // Click the link - are we back on the login page as before?
        elements.linkNeverMind.click();

        // Check the relevant elements
        expect(elements.inputEmail.isEnabled()).toBeFalsy(); // The email address field should be disabled
        expect(elements.password.isDisplayed()).toBeTruthy();
        expect(elements.password.isEnabled()).toBeTruthy();

        // Check that the login button is visible but disabled
        expect(elements.btnLogin.isDisplayed()).toBeTruthy();
        expect(elements.btnLogin.isEnabled()).toBeFalsy();

    });

});
