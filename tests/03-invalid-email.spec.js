'use strict';

// Import libraries
const elements = require('./libraries/elements').elements;
const email = require('./libraries/email-addresses');
const general = require('./libraries/general');

describe('CASE 3: Attempting login with an invalid email address', () => {

    // Declare global constants
    const invalidString = general.generateRandomString(); // Use a random string for our invalid format email address
    const invalidEmailAddress = email.generateEmailAddress(); // Generate an email address to attempt a login with
    const invalidPassword = general.generateRandomString(); // Generate a random (and invalid) password

    beforeAll(() => {
        browser.get(browser.baseUrl);
    });

    it('must not proceed to the password field when the email address is of an invalid format', () => {

        // The 'Next' button should be disabled
        expect(elements.btnNext.isEnabled()).toBeFalsy();

        // Enter our invalid string into the email address field, and double-check that it's invalid
        elements.inputEmail.sendKeys(invalidString);
        elements.inputEmail.getText().then((invalidText) => {
            expect(email.validateEmailAddress(invalidText)).toBeFalsy();
        });

        // The 'Next' button should still be disabled
        expect(elements.btnNext.isEnabled()).toBeFalsy();

    });

    it('must not log in if the email address is of a valid format, but not of a valid user', () => {

        // Attempt a login, and expect a failure
        general.attemptLogin(invalidEmailAddress, invalidPassword, (success) => {
            expect(success).toBeFalsy();
        });

        // Does an error message appear? Check the error text too
        expect(elements.errorMsg.isDisplayed()).toBeTruthy();
        expect(elements.errorMsg.getText()).toContain('Invalid user name, password or permissions.');

    });

});
