'use strict';

// Import libraries
const elements = require('./libraries/elements').elements;
const general = require('./libraries/general');

describe('CASE 4: Attempting login with an invalid password', () => {

    // Declare global constants
    const emailAddress = browser.params.credentials.username; // My email address
    const invalidPassword = general.generateRandomString(); // Generate a random (and invalid) password

    beforeAll(() => {
        browser.get(browser.baseUrl);
    });

    it('must not log in if my password is incorrect', () => {

        // Attempt a login, and expect a failure
        general.attemptLogin(emailAddress, invalidPassword, (success) => {
            expect(success).toBeFalsy();
        });

        // Does an error message appear? Check the error text too
        expect(elements.errorMsg.isDisplayed()).toBeTruthy();
        expect(elements.errorMsg.getText()).toContain('Invalid user name, password or permissions.');

    });

});
