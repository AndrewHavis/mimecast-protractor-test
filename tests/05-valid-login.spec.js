'use strict';

// Import libraries
const general = require('./libraries/general');

describe('CASE 5: Logging in', () => {

    // Declare global constants
    const emailAddress = browser.params.credentials.username; // My email address
    const password = browser.params.credentials.password; // My password
    const token = browser.params.credentials.token; // My authentication token

    beforeAll(() => {
        if (typeof token !== 'undefined') {
            // Use an authentication token to ensure a successful login
            browser.get(browser.baseUrl + '?tkn=' + token + '/login');
        }
        else {
            // Just use the standard URL if no token is given
            browser.get(browser.baseUrl);
        }
    });

    it('must log in if the credentials are valid', () => {

        // Attempt a login, and expect it to succeed
        general.attemptLogin(emailAddress, password, (success) => {
            expect(success).toBeTruthy();
        });

    });

});
