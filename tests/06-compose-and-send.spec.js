'use strict';

// Import libraries
const elements = require('./libraries/elements').elements;

describe('CASE 6: Composing and sending a message', () => {

    // Declare global constants
    const emailAddress = 'example.abc123@mimecast.com'; // The email address to send to
    const subject = 'Test Message'; // The subject of the message
    const bodyText = 'This is a test message.'; // The body text of the message

    // We won't refresh the page here as we should be at a clean state with regards to the inbox

    it('must have the inbox displayed', () => {

        // Check the main content div of the inbox is displayed
        expect(elements.pageContainer.isDisplayed()).toBeTruthy();
        expect(elements.mainContent.isDisplayed()).toBeTruthy();
        expect(elements.messageListTable.isDisplayed()).toBeTruthy();

    });

    it('must compose a message', () => {

        // Click the compose button
        expect(elements.btnCompose.isDisplayed()).toBeTruthy();
        elements.btnCompose.click();

        // Wait for the compose window to load
        browser.wait(() => {
            return elements.bodyContainer.isDisplayed().then((disp) => {
                return disp;
            });
        }, 10000);

        // Check the relevant elements are visible
        expect(elements.sendTo.isDisplayed()).toBeTruthy();
        expect(elements.subject.isDisplayed()).toBeTruthy();
        expect(elements.body.isPresent()).toBeTruthy();

        // Now add in the text to the fields
        elements.sendTo.sendKeys(emailAddress + protractor.Key.ENTER); // Press enter afterwards to accept the email address
        elements.subject.sendKeys(subject);
        elements.bodyContainer.sendKeys(bodyText);

    });

    it('must click the Send button', () => {

        // Click the send button and check for the message that confirms the message has been sent
        expect(elements.btnSend.isDisplayed()).toBeTruthy(); // Check that the send button is visible
        elements.btnSend.click();

    });

    it('must return to the inbox', () => {

        // Can we now see the inbox again?
        expect(elements.messageListTable.isDisplayed()).toBeTruthy();

    });

    it('must have the message we just sent in the Sent Items view', () => {

        // Unfortunately, Protractor doesn't seem to detect the growl message that indicates that the message was sent (it seems to be a timeout issue)
        // Therefore, we'll see if the message exists within Sent Items

        // Go to the Sent Items view
        expect(elements.sentItems.isDisplayed()).toBeTruthy();
        elements.sentItems.click();

        // Now check that the subject of the first item listed matches the one we just sent
        expect(elements.firstSentMsgSubject.getText()).toBe(subject);

    });

});
