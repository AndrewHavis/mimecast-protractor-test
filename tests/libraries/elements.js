'use strict';

// Element definitions
let elementDefs = {};
elementDefs.inputEmail = $('#username');
elementDefs.btnNext = $('button.btn-primary.hidden-xs');
elementDefs.authType = element(by.model('appCtrl.selectedAuthenticationType'));
elementDefs.password = $('#password');
elementDefs.btnLogin = $$('button.btn-primary').get(0);
elementDefs.linkDiffUser = $$('button.btn-link').get(0);
elementDefs.linkForgotPass = $$('button.btn-link').get(1);

// Password reset page
elementDefs.inputResetEmail = element(by.model('forgotPasswordControllerCtrl.username'));
elementDefs.btnResetPassword = $$('button.btn-primary').get(0);
elementDefs.linkNeverMind = $('button.btn-link');

// Error message
elementDefs.errorMsg = $('div.panel-danger > div.text-danger');

// Elements to check for when logged in
elementDefs.pageContainer = $('div.page-container.with-sidebar');
elementDefs.mainContent = $('div.main-content.full-height');
elementDefs.messageListTable = $('table.table-fixed');

// Elements to use when composing and sending a message
elementDefs.btnCompose = $('#main-action > button.btn-primary');
elementDefs.sendTo = $('div.field-to input');
elementDefs.subject = $('div.field-subject input');
elementDefs.body = $('textarea.note-codable');
elementDefs.bodyContainer = $('div.note-editable');
elementDefs.btnRefresh = $('i.icon-arrows-ccw');
elementDefs.btnSend = $('div.btn-toolbar > div.btn-group > button.btn-primary');
elementDefs.msgSent = $('div.growl-container > div.alert-success > div.growl-message');
elementDefs.sentItems = $('#main-menu:nth-child(2)');
elementDefs.firstSentMsgSubject = element.all(by.repeater('item in ctrl.pageResults.displayData track by item.id')).get(0).$('td > ul:nth-of-type(2) > li');

// Return the element definitions
module.exports.elements = elementDefs;
