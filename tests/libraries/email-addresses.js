'use strict';

// Import general functions
const general = require('./general');

// Functions for generating and validating email addresses

module.exports.generateEmailAddress = () => {

    // Generate fake email addresses of the format <random-string>@example.com
    const at = '@';
    const domain = 'example.com';

    // Use a random string for the first (user) part of our email address
    let emailUser = general.generateRandomString();

    // If a dot or underscore is the first or last character of our email address, let's play it safe and replace it with a zero
    if (emailUser[0] === '.' || emailUser[0] === '_') {
        emailUser = '0' + emailUser.substr(1, emailUser.length);
    }
    if (emailUser[emailUser.length - 1] === '.' || emailUser[emailUser.length - 1] === '_') {
        emailUser = emailUser.substr(0, emailUser.length - 1) + '0';
    }

    // Now concatenate our email address components and return it
    let emailAddress = emailUser + at + domain;
    return emailAddress;

};

module.exports.validateEmailAddress = (emailAddress) => {

    // Check the given email address against a given regular expression to ensure its validity
    const emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

    // Return whether the given email address matches the regular expression
    return emailRegex.test(emailAddress);

};
