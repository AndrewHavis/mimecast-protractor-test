'use strict';

// General functions

module.exports.generateRandomString = (lengthOfString) => {

    // Generate a random string
    // If we've specified a length, make it that length, otherwise make it a random length (up to 32 characters)
    const maxLength = 32;
    const useChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._';

    // Initialise our string
    let randomString = '';

    // If we haven't specified a length, use a random length
    if (typeof lengthOfString === 'undefined') {
        lengthOfString = Math.floor(Math.random() * maxLength);
    }

    // If the length is zero, make it one
    if (lengthOfString === 0) {
        lengthOfString = 1;
    }

    // Now loop to create our random string
    for (let i = 0; i < lengthOfString; i++) {
        let charIndex = Math.floor(Math.random() * useChars.length);
        randomString += useChars[charIndex];
    }

    // Now we have our random string, return it
    return randomString;

};

module.exports.attemptLogin = (username, password, callback) => {

    // Attempt to login, and return true if the login is successful, or false otherwise

    // Import element definitions
    const elements = require('./elements').elements;

    // Enter our username, and check that the 'Next' button is enabled and click on it
    elements.inputEmail.clear();
    elements.inputEmail.sendKeys(username);
    expect(elements.btnNext.isEnabled()).toBeTruthy(); // Check the 'Next' button is enabled
    elements.btnNext.click();

    // Wait for the password field to appear
    browser.wait(() => {
        return elements.password.isPresent().then((pres) => {
            return pres;
        });
    }, 10000);

    // Enter our password, and click the login button
    elements.password.sendKeys(password);
    expect(elements.btnLogin.isEnabled()).toBeTruthy();
    elements.btnLogin.click();

    // If an error message appears, return false
    elements.errorMsg.isPresent().then((pres) => {
        if (!!pres) {
            expect(elements.errorMsg.isDisplayed()).toBeTruthy();
            return callback(false);
        }
        else {
            // Wait for the login
            browser.wait(() => {
                return elements.pageContainer.isDisplayed().then((pageDisp) => {
                    return pageDisp;
                });
            }, 15000);
            // Look for the inbox refresh button
            expect(elements.btnRefresh.isDisplayed()).toBeTruthy();
            // Check the main body is visible
            elements.mainContent.isDisplayed().then((contentDisp) => {
                return callback(contentDisp);
            });
        }
    });

};
